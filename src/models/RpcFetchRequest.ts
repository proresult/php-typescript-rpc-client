/*
Copyright 2021, Proresult AS.
License: MIT
*/

export type Method = "GET"|"POST";

export interface RequestOptions {
    credentials?: RequestCredentials;
    cache?: RequestCache;
    redirect?: RequestRedirect;
}

export default class RpcFetchRequest extends Request {
    static defaultHeaders(): Headers {
        return new Headers({
            "Content-Type": "application/json"
        });
    }

    static defaultOptions(): RequestOptions {
        return {
            credentials: "same-origin",
            cache: "no-store",
            redirect: "follow",
        };
    }

    static mergeOptions(o1: RequestOptions, o2: RequestOptions | null): RequestOptions {
        if(o2 === null) {
            return o1;
        } else {
            return Object.assign({}, o1, o2);
        }
    }

    constructor(method: Method, url: URL, body: Blob | null, options: RequestOptions | null = null) {
        const mergedOptions = RpcFetchRequest.mergeOptions(RpcFetchRequest.defaultOptions(), options);
        super(url.toString(),
            // The default options for rpc requests
            {
                headers:  RpcFetchRequest.defaultHeaders(),
                method: method,
                body: body,
                cache: mergedOptions.cache,
                credentials: mergedOptions.credentials,
                redirect: mergedOptions.redirect,
            }
        );
    }

}