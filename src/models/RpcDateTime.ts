/*
Copyright 2021, Proresult AS.
License: MIT
*/

export interface RpcDateTime extends Date {}

export const deserializeRpcDateTime = (data: string): RpcDateTime => new Date(data);