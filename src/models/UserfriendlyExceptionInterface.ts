/*
Copyright 2021, Proresult AS.
License: MIT
*/

import RpcError from "../errors/RpcError";

export default interface UserfriendlyExceptionInterface {
    readonly code: number;
    readonly message: string;
}


export const deserializeUserfriendlyExceptionInterface = (data: any): UserfriendlyExceptionInterface => {
    if(Number.isSafeInteger(data.code) && typeof data.message == "string") {
        return {code: data.code, message: data.message};
    } else {
        throw new RpcError(`Could not deserialize "${data}" into UserfriendlyExceptionInterface`);
    }
}
