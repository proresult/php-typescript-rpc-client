/*
Copyright 2021, Proresult AS.
License: MIT
*/

import UserfriendlyExceptionInterface from "./UserfriendlyExceptionInterface";

export interface RpcResponseContainer {
    readonly response: any|null;
    readonly error: UserfriendlyExceptionInterface|null;
}