/*
Copyright 2021, Proresult AS.
License: MIT
*/

export interface FetchErrorHandling {
    readonly maxRetryCount: number;
}