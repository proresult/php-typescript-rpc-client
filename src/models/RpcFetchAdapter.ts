/*
Copyright 2021, Proresult AS.
License: MIT
*/
import RpcFetchRequest, {Method, RequestOptions} from "../models/RpcFetchRequest";
import RpcError from "../errors/RpcError";
import {FetchErrorHandling} from "./FetchErrorHandling";

export type ParamValue = string|number|boolean;

export default class RpcFetchAdapter<ResponseType> {
    private requestBody: Blob | null = null;
    private path: string;
    private responseTypeMapper: (response: any) => ResponseType;
    private params: Map<string, ParamValue>;
    private requestMethod: Method;
    private _requestOptions: RequestOptions | null;
    private _fetchErrorHandling: FetchErrorHandling | null;

    constructor(
        requestMethod: Method,
        path: string,
        params: Map<string, ParamValue>,
        requestBody: any | null,
        responseTypeMapper: (response: any) => ResponseType,
        requestOptions: RequestOptions | null = null,
        fetchErrorHandling: FetchErrorHandling | null = null,
    ) {
        if(requestBody !== null) {
            this.requestBody = toBlob(requestBody);
        }
        this.path = path;
        this.responseTypeMapper = responseTypeMapper;
        this.params = params;
        this.requestMethod = requestMethod;
        this._requestOptions = requestOptions;
        this._fetchErrorHandling = fetchErrorHandling;
    }

    set requestOptions(ro: RequestOptions | null) {
        this._requestOptions = ro;
    }
    get requestOptions(): RequestOptions | null {
        return this._requestOptions;
    }

    get fetchErrorHandling(): FetchErrorHandling | null {
        return this._fetchErrorHandling;
    }

    addClientCookiePolicy(cookiePolicy: RequestCredentials | null): RpcFetchAdapter<ResponseType> {
        if(cookiePolicy != null) {
            if (this.requestOptions != null) {
                if (this.requestOptions.credentials == undefined) {
                    this.requestOptions.credentials = cookiePolicy;
                } // If this.requestOptions.credentials is set, addClientCookiePolicy will not override it.
            } else {
                this.requestOptions = {credentials: cookiePolicy};
            }
        } // Else do nothing
        return this;
    }

    private paramValToString(v: ParamValue) {
        switch (typeof v) {
            case "string":
                return v;
            case "number":
                return ""+v;
            case "boolean":
                return v ? "true" : "false";
            default:
                throw new RpcError(`Invalid URL parameter value type: ${typeof v}`);
        }
    }

    private makeUrl(baseUrl: URL): URL {
        try {
            const url = new URL(baseUrl.pathname + this.path, baseUrl);
            this.params.forEach((value, key) => {
                const v: string = this.paramValToString(value);
                url.searchParams.append(key, v);
            });
            return url;
        } catch (e) {
            throw new RpcError(`Invalid URL construction: baseUrl= ${baseUrl}, path = ${this.path}`, e);
        }
    }

    request(baseUrl: URL): RpcFetchRequest {
        return new RpcFetchRequest(this.requestMethod, this.makeUrl(baseUrl), this.requestBody, this.requestOptions);
    }

    response(data: any | null): ResponseType {
        return this.responseTypeMapper(data);
    }

}

// @typescript-eslint/no-explicit-any
export const toBlob = (request: any): Blob => new Blob([JSON.stringify(request)]);