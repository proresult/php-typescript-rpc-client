/*
Copyright 2021, Proresult AS.
License: MIT
*/

import AbortError from "../errors/AbortError";

/**
 * A delay function with support for fetch AbortSignal. Throws AbortError if the abortSignal is triggered while waiting.
 * @param milliseconds
 * @param abortSignal
 */
export default function delay(milliseconds: number, abortSignal?: AbortSignal): Promise<number> {
    if(abortSignal?.aborted) {
        return Promise.reject(new AbortError());
    }
    return new Promise((resolve, reject) => {
        const timeoutId = setTimeout(() => {
            resolve(milliseconds);
        }, milliseconds);
        if(abortSignal) {
            abortSignal.addEventListener("abort", () => {
                clearTimeout(timeoutId);
                reject(new AbortError());
            })
        }
    });
}
