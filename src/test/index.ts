/*
Copyright 2021, Proresult AS.
License: MIT
*/

import fetchMock from "fetch-mock";
import {hello} from "@generated/rpc/tests/Rpc/HelloEndpoint";
import {deserializeHelloResponse, HelloResponse} from "@generated/rpc/tests/Models/HelloResponse";
import {RpcDateTime} from "../models/RpcDateTime";
import Client from "../client/Client";
import UserfriendlyExceptionInterface from "../models/UserfriendlyExceptionInterface";
import {RpcResponseContainer} from "../models/RpcResponseContainer";
import UserfriendlyException from "../errors/UserfriendlyException";
import {getSome} from "@generated/rpc/tests/Rpc/GetSomeEndpoint";
import {GetSomeResponse} from "@generated/rpc/tests/Models/GetSomeResponse";
import FetchError from "../errors/FetchError";
import AbortError from "../errors/AbortError";
import delay from "../utils/delay";
import RpcFetchAdapter from "../models/RpcFetchAdapter";
import {FetchErrorHandling} from "../models/FetchErrorHandling";
import { version } from "../../package.json";

const testServerHost = "proresult.app";
const testServerPort = 80;

const testReport = (testName: string, success: boolean): boolean => {
    const body = document.body
    const resultTxt = success ? "OK" : "Failed";
    const msgTxt = `${testName} ${resultTxt}`;
    if(success) {
        console.info(msgTxt);
    } else {
        console.error(msgTxt);
    }
    const resultHtml: HTMLDivElement = document.createElement("div");
    resultHtml.textContent = msgTxt;
    resultHtml.style.color = success ? "green" : "red";
    body.append(resultHtml);
    return success;
}
const pathMaker = (subPath: string): string => `begin:http://${testServerHost}${subPath}`
const responseMaker = (response: any|null, error: UserfriendlyExceptionInterface|null = null): RpcResponseContainer => ({response, error})

const client = new Client(testServerHost, testServerPort, "/rpc");

const helloResponseEqual = (r1: HelloResponse, r2: HelloResponse): boolean => r1.helloTime.getTime() == r2.helloTime.getTime() && r1.message == r2.message
const getSomeResponseEqual = (r1: GetSomeResponse, r2: GetSomeResponse): boolean => r1.txt1 == r2.txt1 && r1.num1 == r2.num1 && r1.txt2 == r2.txt2;

const happyHello = async () => {
    const testName = "happyHello";
    const helloTime: RpcDateTime = new Date();
    fetchMock.post(pathMaker("/rpc/HelloEndpoint/hello"), {
        headers: {"content-type": "application/json"},
        body: responseMaker({message: "Hello, Libby Lompa", helloTime: helloTime}),
    });
    const result = await client.do(hello({name: "Libby Lompa"}));
    const expected: HelloResponse = {
        message: "Hello, Libby Lompa",
        helloTime: helloTime,
    };
    if(!testReport(testName, helloResponseEqual(expected, result))) {
        console.error(`${testName} failed`, expected, result);
    }
}

const unexpectedHello = async () => {
    const testName = "unexpectedHello";
    const helloTime: RpcDateTime = new Date();
    fetchMock.resetBehavior().post(pathMaker("/rpc/HelloEndpoint/hello"), {
        headers: {"content-type": "application/json"},
        body: responseMaker({message: "Unexpected hello", helloTime: helloTime}),
    });
    const result = await client.do(hello({name: "Libby Lompa"}));
    const expected: HelloResponse = {
        message: "Hello, Libby Lompa",
        helloTime: helloTime,
    };
    if(!testReport(testName, !helloResponseEqual(expected, result))) {
        console.error(`${testName} failed. Was equal when it should not be`, expected, result);
    }
}

const wrongContentType = async () => {
    const testName = "wrongContentType";
    const helloTime: RpcDateTime = new Date();

    fetchMock.resetBehavior().post(pathMaker("/rpc/HelloEndpoint/hello"), {
        headers: {"content-type": "text/plain"},
        body: responseMaker({message: "Hello, Libby Lompa", helloTime: helloTime}),
    });
    try {
        await client.do(hello({name: "Libby Lompa"}));
        console.error(`${testName} did not fail as expected`)
        testReport(testName, false);
    } catch (err) {
        const expectedErr = err.name == "ErrorResponse" && err.message.startsWith("Invalid response content-type");
        testReport(testName, expectedErr);
    }
}

const permanentFailure = async () => {
    const testName = "permanentFailure";
    fetchMock.resetBehavior().post(pathMaker("/rpc/HelloEndpoint/hello"), {
        status: 400,
        headers: {"content-type": "application/json"},
        body: responseMaker(null, {code: 400, message: "bad req"}),
    });
    try {
        await client.do(hello({name: "Libby Lompa"}));
        console.error(`${testName} did not fail as expected`)
        testReport(testName, false);
    } catch (err) {
        const expectedErr = err.name == "ErrorResponse" && err.status == 400 && !err.retryable;
        if(!testReport(testName, expectedErr)) {
            console.error("not expected error:", err);
        }
    }
}

const tempFailure = async () => {
    const testName = "tempFailure";
    fetchMock.resetBehavior().post(pathMaker("/rpc/HelloEndpoint/hello"), {
        status: 500,
        headers: {"content-type": "application/json"},
        body: responseMaker(null, {code: 500, message: "server err"}),
    });
    try {
        await client.do(hello({name: "Libby Lompa"}));
        console.error(`${testName} did not fail as expected`)
        testReport(testName, false);
    } catch (err) {
        const expectedErr = err.name == "ErrorResponse" && err.status == 500 && err.retryable;
        if(!testReport(testName, expectedErr)) {
            console.error("not expected error:", err);
        }
    }
}

const userfriendlyException = async() => {
    const testName = "userfriendlyException";
    fetchMock.resetBehavior().post(pathMaker("/rpc/HelloEndpoint/hello"), {
        status: 200,
        headers: {"content-type": "application/json"},
        body: responseMaker(null, {code: 1234, message: "You messed up!"}),
    });
    try {
        await client.do(hello({name: "Failure Imminent"}));
        console.error(`${testName} did not fail as expected`)
        testReport(testName, false);
    } catch (err) {
        const expectedErr = err.name == "UserfriendlyException" && err.code == 1234;
        if(!testReport(testName, expectedErr)) {
            console.error("not expected error:", err);
        }
    }
}

const happyGetSome = async () => {
    const testName = "happyGetSome";
    const expected = {txt1: "txt1", "num1": 1234, txt2: "txt2"}
    fetchMock.resetBehavior().get(pathMaker("/rpc/GetSomeEndpoint/getSome"), {
        status: 200,
        headers: {"content-type": "application/json"},
        body: responseMaker(expected)
    });
    const result = await client.do(getSome("txt1", 1234, "txt2"));
    if(!testReport(testName, getSomeResponseEqual(result, expected))) {
        console.error(`${testName} failed. Was not equal when it should be`, expected, result);
    }
}

const withHeaders = async () => {
    const testName = "withHeaders";
    const expected = {txt1: "txt1", "num1": 1234, txt2: "txt2"}
    fetchMock.resetBehavior().get(pathMaker("/rpc/GetSomeEndpoint/getSome"), {
        status: 200,
        headers: {"content-type": "application/json"},
        body: responseMaker(expected)
    });
    const requestId = "334455";
    const headerKey = "X-request-id";
    client.requestHeaders.set(headerKey, requestId);
    await client.do(getSome("txt1", 1234, "txt2"));
    const lastCall = fetchMock.lastCall();
    if(lastCall) {
        const ok = lastCall.request?.headers.get(headerKey) === requestId;
        if(!testReport(testName, ok)) {
            console.error(`${testName} failed. Request header value not found or not equal input`);
        }
    }
}

const serverGone = async () => {
    const testName = "serverGone";
    fetchMock.reset();
    try {
        console.info(`Expect error logging (from fetch) below`);
        await client.do(hello({name: "Will Fail"}));
        // If we get here test failed since no error was thrown
        console.error(`${testName} did not throw expected exception`);
        testReport(testName, false);
    } catch (err) {
        const ok = err instanceof FetchError &&
            !!err.causedBy &&
            err.request.method === "POST" &&
            err.request.url === `http://${testServerHost}/rpc/HelloEndpoint/hello`
        if(!ok) {
            console.error(`${testName} got unexpected error response`, err)
        }
        testReport(testName, ok);
    }
}

const abortSignal = async () => {
    const ac = new AbortController();
    const delay = 500;

    fetchMock.reset();

    const expected: HelloResponse = { message: 'Hello', helloTime: new Date() };

    fetchMock.post(pathMaker("/rpc/HelloEndpoint/hello"), {
        status: 200,
        headers: {"content-type": "application/json"},
        body: responseMaker(expected),
    },{ delay });

    try {
        setTimeout(() => ac.abort(), delay / 2);
        await client.do(hello({ name: 'Im in it for the long run' }), ac.signal);
        
        testReport(AbortSignal.name, false);

    } catch (error) {
        const ok = error instanceof AbortError;
        testReport(AbortSignal.name, ok);
    }
}

// Test that aborting a delay call with abort signal works
const delayWithAbort = async () => {
    let beforeTime = new Date();
    let waitedMillis = await delay(100);
    let afterTime = new Date();
    const timeOk = (beforeTime: Date, waitedMillis: number, afterTime: Date) => {
        const measuredWaitedMillis = afterTime.getTime() - beforeTime.getTime();
        return !(measuredWaitedMillis < waitedMillis - 10 || measuredWaitedMillis > waitedMillis + 10);
    }
    testReport("delay time measured", timeOk(beforeTime, waitedMillis, afterTime));
    // Test that an abort signal already triggered makes delay abort immediately
    let aborter = new AbortController();
    aborter.abort(); // Abort it immediately for testing purposes
    beforeTime = new Date();
    try {
        await delay(100, aborter.signal);
        afterTime = new Date();
    } catch (err) {
        testReport("delay aborted", err instanceof AbortError);
        afterTime = new Date();
    }
    testReport("delay aborted time", timeOk(beforeTime, 0, afterTime));
    // Test an abort signal triggered after delay has started aborts the delay immediately
    aborter = new AbortController();
    beforeTime = new Date();
    try {
        const waitedMillisPromise = delay(3000, aborter.signal);
        await delay(30);
        aborter.abort(); // Trigger abort after 30 ms.
        waitedMillis = await waitedMillisPromise;
        testReport("delay abort - no error", false); // Test failed if we get here (expecting error throw)
    } catch (err) {
        afterTime = new Date();
        waitedMillis = afterTime.getTime() - beforeTime.getTime();
        testReport("delay abort", err instanceof AbortError && timeOk(beforeTime, waitedMillis, afterTime) && timeOk(beforeTime, 30, afterTime));
    }
}

const defaultHelloReq = () => hello({name: "Libby Lompa"});

const retryFailedOnce = async () => {
    const testName = "retryFailedOnce";
    fetchMock.resetBehavior().post(pathMaker("/rpc/HelloEndpoint/hello"), {
        status: 503,
        headers: {"content-type": "application/json", "Retry-After": "0"},
        body: responseMaker(null, {code: 503, message: "server temp unavail"}),
    });
    try {
        await client.do(defaultHelloReq());
        console.error(`${testName} did not fail as expected`)
        testReport(testName, false);
    } catch (err) {
        const expectedErr = err.name == "ErrorResponse" && err.status == 503 && err.retryable && err.retryNumber === 1; // <= Checks that the error thrown was after one retry
        if(!testReport(testName, expectedErr)) {
            console.error("not expected error:", err);
        }
        // Check that last request had Retry-Number header set
        const lastReq = fetchMock.lastCall();
        testReport(testName + "/retry-number", lastReq?.request?.headers.get("Retry-Number") === "1");
    }
}

// Test that a client call with abortsignal retries 10 times on retryable failure.
const retryFailedWithAbort = async () => {
    const testName = "retryFailedWithAbort";
    const aborter = new AbortController();
    fetchMock.resetBehavior().post(pathMaker("/rpc/HelloEndpoint/hello"), {
        status: 503,
        headers: {"content-type": "application/json", "Retry-After": "0"},
        body: responseMaker(null, {code: 503, message: "server temp unavail"}),
    });
    try {
        await client.do(defaultHelloReq(), aborter.signal);
        console.error(`${testName} did not fail as expected`)
        testReport(testName, false);
    } catch (err) {
        const expectedErr = err.name == "ErrorResponse" && err.status == 503 && err.retryable && err.retryNumber === 10; // <= Checks that the error thrown was after 10 retries
        if(!testReport(testName, expectedErr)) {
            console.error("not expected error:", err);
        }
        // Check that last request had Retry-Number header set
        const lastReq = fetchMock.lastCall();
        testReport(testName + "/retry-number", lastReq?.request?.headers.get("Retry-Number") === "10");
    }
}

// Test that a fetch error gets thrown as it should
const fetchError = async () => {
    const testName = "fetchError";
    fetchMock.resetBehavior().post(pathMaker("/rpc/HelloEndpoint/hello"), {
        throws: new Error("test failure")
    });
    try {
        await client.do(defaultHelloReq());
        console.error(`${testName} did not fail as expected`);
        testReport(testName, false);
    } catch (err) {
        const expectedErr = err.name == "FetchError" && err instanceof FetchError;
        if(!testReport(testName, expectedErr)) {
            console.error("not expected error:", err);
        }
    }
}

// Test that a fetch error (network failure) retries as it should
const retryFetchError = async () => {
    const testName = "retryFetchError"
    fetchMock.resetBehavior().post(pathMaker("/rpc/HelloEndpoint/hello"), (url, opts) => {
        // @ts-ignore Couldn't figure out the headers type
        const retryNumber = Number(opts.headers ["retry-number"]) || 0;
        if (retryNumber === 2) {
            const helloResp: HelloResponse = {
                message: "Hello, Postman Pat",
                helloTime: new Date(),
            };
            return {
                status: 200,
                headers: {"content-type": "application/json"},
                body: responseMaker(helloResp),
            }
        }
        throw new Error("test failure");
    });
    const fetchErrorHandling: FetchErrorHandling = {maxRetryCount: 2}
    const req = new RpcFetchAdapter<HelloResponse>("POST", "/HelloEndpoint/hello", new Map(), {name: "Postman Pat"}, deserializeHelloResponse, null, fetchErrorHandling);
    await client.do(req);
    // Check that the request succeeded after two retries
    const lastReq = fetchMock.lastCall();
    testReport(testName, lastReq?.request?.headers.get("Retry-Number") === "2");
}

// Test that client version is included in request headers
const clientVersionHeader = async () => {
    const testName = "clientVersionHeader";
    fetchMock.resetBehavior().post(pathMaker("/rpc/HelloEndpoint/hello"), {
        headers: {"content-type": "application/json"},
        body: responseMaker({message: "Hello, Libby Lompa", helloTime: new Date()}),
    });
    await client.do(defaultHelloReq());
    const lastReq = fetchMock.lastCall();
    testReport(testName, lastReq?.request?.headers.get(Client.VersionHeaderKey) === version);
}


const allTests = async() => {
    await happyHello();
    await unexpectedHello();
    await wrongContentType();
    await permanentFailure();
    await tempFailure();
    await userfriendlyException();
    await happyGetSome();
    await withHeaders();
    await serverGone();
    await abortSignal();
    await delayWithAbort();
    await retryFailedOnce();
    await retryFailedWithAbort();
    await fetchError();
    await retryFetchError();
    await clientVersionHeader();
}

allTests().catch(e => {
    throw e;
})