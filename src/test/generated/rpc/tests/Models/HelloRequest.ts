

export interface HelloRequest {
    name: string
}
export const deserializeHelloRequest = (d: any): HelloRequest => ({
name: d.name,
});
