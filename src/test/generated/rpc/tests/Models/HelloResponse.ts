import {RpcDateTime, deserializeRpcDateTime} from "../../../../../models/RpcDateTime";

export interface HelloResponse {
    message: string;
	helloTime: RpcDateTime
}
export const deserializeHelloResponse = (d: any): HelloResponse => ({
message: d.message,
helloTime: deserializeRpcDateTime(d.helloTime),
});
