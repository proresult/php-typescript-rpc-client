/*
Copyright 2021, Proresult AS.
License: MIT
*/

export interface GetSomeResponse {
    txt1: string;
    num1: number;
    txt2: string;
}
export const deserializeGetSomeResponse = (d: any): GetSomeResponse => ({
    txt1: d.txt1,
    num1: d.num1,
    txt2: d.txt2,
});