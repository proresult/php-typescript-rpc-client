import { HelloRequest } from '../Models/HelloRequest';
import { HelloResponse, deserializeHelloResponse } from '../Models/HelloResponse';
import RpcFetchAdapter, {ParamValue} from "../../../../../models/RpcFetchAdapter";

export const hello = (request: HelloRequest): RpcFetchAdapter<HelloResponse> => {
    const params = new Map<string, ParamValue>();
    return new RpcFetchAdapter<HelloResponse>("POST", "/HelloEndpoint/hello", params, request, deserializeHelloResponse);
}
