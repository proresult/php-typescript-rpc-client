/*
Copyright 2021, Proresult AS.
License: MIT
*/

import {deserializeGetSomeResponse, GetSomeResponse} from "../Models/GetSomeResponse";
import RpcFetchAdapter, {ParamValue} from "../../../../../models/RpcFetchAdapter";

export const getSome = (txt1: string, num1: number, txt2?: string): RpcFetchAdapter<GetSomeResponse> => {
    const params = new Map<string, ParamValue>();
    params.set("txt1", txt1);
    params.set("num1", num1);
    if(txt2 !== undefined) {
        params.set("txt2", txt2);
    }
    return new RpcFetchAdapter<GetSomeResponse>("GET", "/GetSomeEndpoint/getSome", params, null, deserializeGetSomeResponse);
}