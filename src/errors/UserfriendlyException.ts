/*
Copyright 2021, Proresult AS.
License: MIT
*/

import UserfriendlyExceptionInterface from "../models/UserfriendlyExceptionInterface";

/**
 * Exception that originates in server RPC processing, with info propagated from there.
 */
export default class UserfriendlyException extends Error {
    private _code: number;

    constructor(data: UserfriendlyExceptionInterface) {
        super(data.message);
        this._code = data.code;

        // Think this is some boilerplate that helps get a good error experience in javascript.

        // @ts-ignore
        if(Error.captureStackTrace) {
            // @ts-ignore
            Error.captureStackTrace(this, UserfriendlyException);
        }

        this.name = "UserfriendlyException";
    }

    get code() {
        return this._code;
    }
}