/*
Copyright 2021, Proresult AS.
License: MIT
*/

import RpcFetchRequest from "../models/RpcFetchRequest";

/**
 * Thrown by client when fetch throws.
 * (Because of missing network connection, dns failure, etc)
 */
export default class FetchError extends Error {
    readonly request: RpcFetchRequest;
    readonly causedBy?: TypeError;

    constructor(request: RpcFetchRequest, causedBy?: TypeError) {
        super(`rpc fetch network request failed (${request.method} => ${request.url})`);
        this.request = request;
        this.causedBy = causedBy;

        // @ts-ignore
        if(Error.captureStackTrace) {
            // @ts-ignore
            Error.captureStackTrace(this, FetchError);
        }

        this.name = "FetchError";
    }
}