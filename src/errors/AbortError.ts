/*
Copyright 2021, Proresult AS.
License: MIT
*/
export default class AbortError extends Error {
    private _previous: Error | null = null;

    /**
     * AbortError is thrown when an AbortSignal is triggered. (While waiting for a fetch result).
     */
    constructor() {
        super('The user aborted the request.');

        // @ts-ignore
        if(Error.captureStackTrace) {
            // @ts-ignore
            Error.captureStackTrace(this, AbortError);
        }

        this.name = "AbortError";
    }

    /**
     * @param previous If the Abort happens while waiting for a retry of a failed fetch, the original fetch error that caused retry is set here.
     */
    set previous(previous: Error | null) {
        this._previous = previous;
    }

    /**
     * @return If the abort happened during waiting for a retry of a failed fetch, the original fetch error that caused retry is returned here.
     */
    get previous() {
        return this._previous;
    }
}