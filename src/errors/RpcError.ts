/*
Copyright 2021, Proresult AS.
License: MIT
*/
export default class RpcError extends Error {

    public causedBy?: Error;

    constructor(message: string, causedBy?: Error) {
        let str = message;
        if(causedBy) {
            str += `\nCaused by: ${causedBy}`;
        }
        super(str);
        this.causedBy = causedBy;

        // @ts-ignore
        if(Error.captureStackTrace) {
            // @ts-ignore
            Error.captureStackTrace(this, RpcError);
        }

        this.name = "RpcError";

    }
}