/*
Copyright 2021, Proresult AS.
License: MIT
*/

import RpcFetchRequest from "../models/RpcFetchRequest";

/**
 * Thrown by Client when server responds with statuscode outside 200-299 range.
 */
export default class ErrorResponse extends Error {
    private _request: RpcFetchRequest;
    private _response: Response;
    private _retryNumber: number;

    // http status codes that indicate a error that might go away if we retry
    static readonly retryableStatuses = [408, 429, 500, 502, 503];

    constructor(request: RpcFetchRequest, response: Response, message: string, retryNumber: number) {
        super(message);
        this._request = request;
        this._response = response;
        this._retryNumber = retryNumber;

        // Think this is some boilerplate that helps get a good error experience in javascript.

        // @ts-ignore
        if(Error.captureStackTrace) {
            // @ts-ignore
            Error.captureStackTrace(this, ErrorResponse);
        }

        this.name = "ErrorResponse";
    }

    get status(): number {
        return this._response.status;
    }

    get retryable(): boolean {
        return ErrorResponse.retryableStatuses.includes(this.status);
    }

    /**
     * If there is a Retry-After header in the passed response, it's value is returned.
     */
    static retryAfter(response: Response): number | Date | null {
        const retryAfterDateOrSeconds = response.headers.get("Retry-After");
        if (retryAfterDateOrSeconds !== null && retryAfterDateOrSeconds.length > 0) {
            const retryAfterSeconds = Number(retryAfterDateOrSeconds);
            if (isFinite(retryAfterSeconds)) {
                return retryAfterSeconds;
            }
            const retryAfterDate = new Date(retryAfterDateOrSeconds);
            if (isFinite(retryAfterDate.getTime())) {
                return retryAfterDate;
            }
        }
        return null;
    }

    /**
     * If there is a Retry-After header in this error response, it's value is returned.
     */
    get retryAfter(): number | Date | null {
        return ErrorResponse.retryAfter(this.response);
    }

    /**
     * Converts any date passed in retryAfter to number of seconds from now.
     */
    static retryAfterSeconds(retryAfter: number | Date | null): number | null {
        if (retryAfter instanceof Date) {
            const now = new Date();
            const seconds = Math.round((retryAfter.getTime() - now.getTime()) / 1000);
            if (seconds < 0) {
                return 0;
            }
            return seconds;
        }
        return retryAfter;
    }

    /**
     * Converts any date returned from this.retryAfter to number of seconds from now.
     */
    get retryAfterSeconds(): number | null {
        return ErrorResponse.retryAfterSeconds(this.retryAfter);
    }

    /**
     * How many times the request was retried before this error was returned.
     */
    get retryNumber(): number {
        return this._retryNumber;
    }

    get request() {
        return this._request;
    }

    get response() {
        return this._response;
    }
}