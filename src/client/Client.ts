/*
Copyright 2021, Proresult AS.
License: MIT
*/
import RpcFetchAdapter from "../models/RpcFetchAdapter";
import ErrorResponse from "../errors/ErrorResponse";
import {RpcResponseContainer} from "../models/RpcResponseContainer";
import UserfriendlyException from "../errors/UserfriendlyException";
import RpcError from "../errors/RpcError";
import UserfriendlyExceptionInterface, {deserializeUserfriendlyExceptionInterface} from "../models/UserfriendlyExceptionInterface";
import FetchError from "../errors/FetchError";
import AbortError from "../errors/AbortError";
import delay from "../utils/delay";
import { version } from "../../package.json";

export default class Client {
    public baseUrl: URL;
    private cookiePolicy: RequestCredentials | null;
    private _requestHeaders: Headers | null = null; // Headers set on client gets added to all subsequent requests (but does not override headers already set on request)
    public static readonly VersionHeaderKey = "X-Client-Version";

    constructor(host: string | null = null, port: number | null = null, basePath: string | null = null, cookiePolicy: RequestCredentials | null = null) {
        // Use current location as default base, then change it accordingly to params
        this.baseUrl = Client.defaultBaseUrl();
        const portStr = port != null && port > 0 ? ""+port : "";
        if(host != null) {
            this.baseUrl.host = host;
        }
        if(port != null) {
            this.baseUrl.port = portStr;
        }
        if(basePath != null) {
            if(!basePath.startsWith("/")) {
                throw new Error(`Client basePath must start with \"/\". Got: ${basePath}`);
            }
            if(basePath.endsWith("/")) {
                throw new Error(`Client basePath must NOT end with \"/\". Got: ${basePath}`);
            }
            this.baseUrl.pathname = basePath;
        }
        this.cookiePolicy = cookiePolicy;
        this.requestHeaders.set(Client.VersionHeaderKey, version);
    }

    public setProtocol(protocol: string): void {
        this.baseUrl.protocol = protocol;
    }
    public get requestHeaders(): Headers {
        if(this._requestHeaders === null) {
            this._requestHeaders = new Headers({});
        }
        return this._requestHeaders;
    }

    /**
     * Returns current (window) location object as a URL, for use as baseUrl when it is not specified
     */
    static defaultBaseUrl(): URL {
        const url = new URL("", location as unknown as URL);
        url.pathname = "/"; // Default base url has the root path.
        url.search = ""; // Default base url has no query arguments.
        return url;
    }

    protected async doFetch(request: Request, signal?: AbortSignal): Promise<Response> {
        try {
            return await fetch(request, { signal });
        } catch (err) {
            if (signal?.aborted) {
                throw new AbortError();
            }

            throw new FetchError(request, err);
        }
    }

    protected rpcResponseContainer(responseJson: any): RpcResponseContainer {
        let response: any | null = null;
        let error: UserfriendlyExceptionInterface | null = null;
        if(responseJson.error) {
            error = deserializeUserfriendlyExceptionInterface(responseJson.error);
        }
        if(responseJson.response != undefined) {
            response = responseJson.response;
        }
        if(error != null && response != null) {
            throw new RpcError(`Both error and response properties was set in RpcResponseContainer data returned.`);
        }
        if(error == null && response == null) {
            throw new RpcError(`Unexpected response format. Neither error or response property found.`);
        }
        return {response, error}
    }

    /**
     * Performs the rpc request without any error capture/retry.
     *
     * @protected
     */
    protected async internalDo<ResponseType>(rpc: RpcFetchAdapter<ResponseType>, signal?: AbortSignal, retryNumber : number = 0): Promise<ResponseType> {
        const request = rpc.addClientCookiePolicy(this.cookiePolicy).request(this.baseUrl);
        if(retryNumber > 0) { // Add Retry-Number header info to request in case the server is interested in knowing that this is a retry of a failed request.
            request.headers.set("Retry-Number", `${retryNumber}`);
        }
        // Request headers set on the client gets added to the request.
        // If a header is already set on the request it is NOT overwritten
        if(this._requestHeaders !== null) {
            for(const [key, val] of this._requestHeaders) {
                if(!request.headers.has(key)) {
                    request.headers.set(key, val);
                }
            }
        }
        const response = await this.doFetch(request, signal);
        if(response.ok) {
            const contentType = response.headers.get("content-type");
            if(contentType == "application/json") {
                const responseContainer = this.rpcResponseContainer(await response.json());
                // Determine if response is expected rpc response (of ResponseType), or a "OkException" propagated from server.
                if(responseContainer.error) {
                    throw new UserfriendlyException(responseContainer.error);
                }
                return rpc.response(responseContainer.response);
            } else {
                throw new ErrorResponse(request, response, `Invalid response content-type: ${contentType}. Should be "application/json"`, retryNumber);
            }
        } else {
            // handle error responses
            const message = await response.text();
            throw new ErrorResponse(request, response, message, retryNumber);
        }
    }

    protected async retryDelay(delaySeconds: number, error: Error, signal?: AbortSignal): Promise<void> {
        try {
            const retryJitterMillis = Math.floor(Math.random() * 200); // Add random wait between 0 and 200 milliseconds before retry
            await delay((delaySeconds * 1000) + retryJitterMillis, signal);
        } catch (delayError) {
            // If error is an AbortError (the only expected error here), add the cause of the retry to it before rethrowing it.
            if (delayError instanceof AbortError) {
                delayError.previous = error;
            }
            throw delayError;
        }
    }

    /**
     * Performs the rpc request and retries retryable errors up to desired amount of times.
     *
     * @protected
     */
    protected async retryingDo<ResponseType>(rpc: RpcFetchAdapter<ResponseType>, signal?: AbortSignal, retryNumber : number = 0): Promise<ResponseType> {
        try {
            return await this.internalDo(rpc, signal, retryNumber);
        } catch (error) {
            if (error instanceof ErrorResponse && error.retryable && error.retryAfter !== null) {
                // Wait so long as retryAfter says, then retry.
                // XXX We might want to add more (configurable?) number of retries here later.
                await this.retryDelay(error.retryAfterSeconds ?? 0, error, signal);
                if (signal && retryNumber < 9) {
                    // When AbortSignal is provided, we want to retry retryable errors up to ten times, since the caller can abort when he wants.
                    return await this.retryingDo(rpc, signal, error.retryNumber + 1);
                } else {
                    // In the normal case or if we've retried 9 times, try one more time and fail if that doesn't work.
                    return await this.internalDo(rpc, signal, error.retryNumber + 1);
                }
            } else if (error instanceof FetchError && rpc.fetchErrorHandling !== null && rpc.fetchErrorHandling.maxRetryCount > retryNumber) {
                await this.retryDelay(retryNumber + 1, error, signal);
                return await this.retryingDo(rpc, signal, retryNumber + 1);
            } else {
                throw error;
            }
        }
    }

    /**
     * Performs the rpc request described in the given RpcFetchAdapter, and returns the result of it deserialized to the expected type described in the RpcFetchAdapter.
     *
     * The client will retry requests if the error response indicates that it is retryable and has a "retryAfter" time set. Normally it will only retry once. If an AbortSignal is
     * provided, it will retry up to ten times, since the abortsignal indicates that the caller takes responsibility for cancelling if the request takes too long.
     *
     * @param rpc The RpcFetchAdapter that describes what rpc to perform, the http method to use, what argument(s) to pass to it and what response to expect.
     * @param signal If an AbortSignal is given, it lets the caller abort the request at any time. It also means the client will retry retryable failures several times.
     */
    async do<ResponseType>(rpc: RpcFetchAdapter<ResponseType>, signal?: AbortSignal): Promise<ResponseType> {
        return await this.retryingDo(rpc, signal, 0);
    }

}